# Alternative WissKI Navigate Page

This module adds another navigate page. This page containes links to views, which can be selected for every Drupal installation individually, even views, which are not related to WissKI. The output can be customized with a few different display options (for example tiles) and different labels. The order can be specified, too.
The breadcrumb links on the wisski_views pages and on the wisski/navigate pages are changed to the new browsing page.

### Installing

Put the module folder into /modules/custom and activate it under /admin/modules.
Please note there are some requriments for using this module. Most of all views and WissKI have to be installed.

### Further instructions

The newly generated page can be found under /wisski-browse. The module will create a link for the main menu, which can be changed, too. The settings are under /admin/config/system. Please note the customized labels can only be translated once the settings form has been saved at least once. Only customized labels can be translated on this page.
There are different display options available which you can select on the modules settings page.
