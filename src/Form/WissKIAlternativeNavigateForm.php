<?php

namespace Drupal\wisski_alternative_navigate\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;

class WissKIAlternativeNavigateForm extends ConfigFormBase {

  public function getFormId() {
    return 'wisski_alternative_navigate_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('wisski_alternative_navigate.adminsettings');
    $display_views = $config->get('display_views');

    $entity_ids = \Drupal::entityQuery('view')
      ->condition('status', TRUE)
      ->execute();

    $form['display_option'] = array(
       '#title' => t('Display options'),
       '#description' => t('Activate a display option for your navigate page.'),
       '#type' => 'radios',
       '#default_value' => $config->get('display_option'),
       '#options' => array(0 => t('List'), 
                           1 => t('List with administrative description'), 
			   2 => t('List with preview images'),
			   3 => t('Tile look with preview images'),
			   4 => t('Porthole look with preview images'),
		          ),
    );

    $form['display_views'] = array(
       '#type' => 'table',
       '#caption' => $this->t('Select the views you want to link on your navigate site. Please note you can only choose views which are base on the WissKI main bundles or which have at least one page. Views of other WissKI groups or without a page will not be listed.'),
       '#header' => array(
                 $this
                    ->t('Activate'),
                 $this
		    ->t('View ID'),
		 $this
		    ->t('Original Label'),
		 $this
          	    ->t('New Label'),
		 $this
                    ->t('Preview Image'),
		 $this
                    ->t('Page'),
		 $this
		    ->t('Weight'),

       ),
    );

    foreach($entity_ids as $entity_id) {
       $view = \Drupal::service('entity_type.manager')
            ->getStorage('view')
	    ->load($entity_id);

//       echo '<pre>' , var_dump($view->get('display')), '</pre>';


       //Hier werden die verschiedenen Seiten, die eine Ansicht haben kann, geholt. Blöcke oder anderes werden nicht berücksichtigt
       $view_pages = array();

       foreach($view->get('display') as $view_display) {
	       if($view_display['id'] == $entity_id || substr( $view_display['id'], 0, 5 ) === "page_" || $view->get('base_table') == "wisski_individual") {
		       $view_page_id = $view_display['id'];
		       $view_page_title = $view_display['display_title'];
		       $view_pages[$view_page_id] = $view_page_title; 
	       }
//	       echo '<pre>' , var_dump($display_views) , '</pre>';
	       //Die Default-Seite wird festgelegt. Entweder die gespeicherte Version aus der Datenbank oder aber die erste verfügbare Seite
               if(isset($display_views[$entity_id]['page'])) {
		       $view_default_page = $display_views[$entity_id]['page'];
               }
	       else {
                   if($view_display['id'] == $entity_id) {
			   $view_default_page = $entity_id;
		   }
		   elseif($view->get('base_table') == "wisski_individual") {
			   $view_default_page = $view_display['id'];
		   }
		   else {
			   $view_default_page = "page_1";
	           }
	       }
//	       echo $view->label()." - ".$view_default_page."<br>";
//	       echo $view->label()." - ".$view_default_page."<br>";
	       //echo $view_display['id']." - ".$view_display['display_title']." - ".$view_default_page."<br>";


       }


       if(isset($display_views[$entity_id]['activate'])) {
	       $view_activate = $display_views[$entity_id]['activate'];
       }
       else {
	       $view_activate = $config->get('field.activate');
       }

       if(isset($display_views[$entity_id]['new_label'])) {
               $view_new_label = $display_views[$entity_id]['new_label'];
       }
       else {
               $view_new_label = $config->get('field.new_label');
       }

       if(isset($display_views[$entity_id]['weight'])) {
               $view_weight = $display_views[$entity_id]['weight'];
       }
       else {
               $view_weight = $config->get('field.weight');
       }
       if(isset($display_views[$entity_id]['preview_image'])) {
               $view_image = $display_views[$entity_id]['preview_image'];
       }
       else {
               $view_image = $config->get('field.preview_image');
       }


       if($view->get('base_table') == "wisski_individual") {
		    $activate = $entity_id.".activate";
                    $form['display_views'][$entity_id]['activate'] = array(
                         '#type' => 'checkbox',
                         '#default_value' => $view_activate,
                    );
                    $form['display_views'][$entity_id]['view_id'] = array(
                         '#type' => 'textfield',
                	 '#default_value' => $entity_id,
                	 '#title' => $this
                         ->t('View ID'),
                         '#title_display' => 'invisible',
			 '#disabled' => 'true',
			 '#size' => '30',
                    );
                    $form['display_views'][$entity_id]['original_label'] = array(
                         '#type' => 'textfield',
                	 '#default_value' => $view->label(),
                	 '#title' => $this
                         ->t('Original Label'),
	                 '#title_display' => 'invisible',
			 '#disabled' => 'true',
			 '#size' => '30',
                    );
                    $form['display_views'][$entity_id]['new_label'] = array(
	                 '#type' => 'textfield',
                 	 '#default_value' => $view_new_label,
                         '#title' => $this
                          ->t('New Label'),
			 '#title_display' => 'invisible',
			 '#size' => '40',
		    );
                    $form['display_views'][$entity_id]['preview_image'] = array(
                        '#type' => 'managed_file',
                        '#title' => t('Choose file'),
                        '#title_display' => 'invisible',
                        '#upload_location' => 'public://alternative-navigate/',
                        '#default_value' => $view_image,
                        '#upload_validators' => array(
                               'file_validate_extensions' => array('gif png jpg jpeg'),
                        ),
                        '#states' => array(
                             'visible' => array(
                             ':input[name="File_type"]' => array('value' => t('Upload Your File')),
                             ),
                        ),
                    );
                    $form['display_views'][$entity_id]['page'] = array(
                         '#type' => 'radios',
			 '#default_value' => $view_default_page,
			 '#options' => $view_pages,
                         '#title' => $this
                          ->t('Page'),
                         '#title_display' => 'invisible',
         	    );
                    $form['display_views'][$entity_id]['weight'] = array(
                         '#type' => 'weight',
                         '#default_value' => $view_weight,
                         '#delta' => 10,
                    );
       }
       else {
	     if(isset($view->get('display')['page_1'])) {
                    $form['display_views'][$entity_id]['activate'] = array(
                         '#type' => 'checkbox',
                         '#default_value' => $view_activate,
                    );
                    $form['display_views'][$entity_id]['view_id'] = array(
                         '#type' => 'textfield',
                         '#default_value' => $entity_id,
                         '#title' => $this
                         ->t('View ID'),
                         '#title_display' => 'invisible',
			 '#disabled' => 'true',
			 '#size' => '30',
                    );
                    $form['display_views'][$entity_id]['original_label'] = array(
                         '#type' => 'textfield',
                         '#default_value' => $view->label(),
                         '#title' => $this
                         ->t('Original Label'),
                         '#title_display' => 'invisible',
			 '#disabled' => 'true',
			 '#size' => '30',
                    );
                    $form['display_views'][$entity_id]['new_label'] = array(
                         '#type' => 'textfield',
                         '#default_value' => $view_new_label,
                         '#title' => $this
                          ->t('New Label'),
			  '#title_display' => 'invisible',
			  '#size' => '40',
		    );
                    $form['display_views'][$entity_id]['preview_image'] = array(
                        '#type' => 'managed_file',
                        '#title' => t('Choose file'),
                        '#title_display' => 'invisible',
                        '#upload_location' => 'public://alternative-navigate/',
                        '#default_value' => $view_image,
                        '#upload_validators' => array(
                               'file_validate_extensions' => array('gif png jpg jpeg'),
                        ),
                        '#states' => array(
                             'visible' => array(
                             ':input[name="File_type"]' => array('value' => t('Upload Your File')),
                             ),
                        ),
                    );
                    $form['display_views'][$entity_id]['page'] = array(
                         '#type' => 'radios',
                         '#default_value' => $view_default_page,
                         '#options' => $view_pages,
                         '#title' => $this
                          ->t('Page'),
                         '#title_display' => 'invisible',
                    );
                    $form['display_views'][$entity_id]['weight'] = array(
                         '#type' => 'weight',
                         '#default_value' => $view_weight,
                         '#delta' => 10,
                    );
              }
       }
    }

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('wisski_alternative_navigate.adminsettings');
    $config->set('display_views', $form_state->getValue('display_views'));
    $config->set('display_option', $form_state->getValue('display_option'));
    $config->save();

    foreach($form_state->getValue('display_views') as $view)
      {
          if (!empty($view['preview_image'])) {
		 $view_id = $view['view_id'];     
		 $view_image_id = $view['preview_image'];
		 $file = \Drupal\file\Entity\File::load($view_image_id[0]);
                 $file_parts = pathinfo($file->getFilename());
                 $file_extension = $file_parts['extension'];
                 rename($file->getFileUri(),"public://alternative-navigate/preview_image_".$view_id.".".$file_extension);
                 $file->setFilename("preview_image_".$view_id.".".$file_extension);
                 $file->setFileUri("public://alternative-navigate/preview_image_".$view_id.".".$file_extension);
                 $file->setPermanent();
                 $file->save();
         }
      }
    
    parent::submitForm($form, $form_state);
  }

  protected function getEditableConfigNames() {
    return ['wisski_alternative_navigate.adminsettings'];
  }

}
