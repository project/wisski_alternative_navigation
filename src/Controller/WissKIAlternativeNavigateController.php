<?php

namespace Drupal\wisski_alternative_navigate\Controller;

use Drupal\Core\Controller\ControllerBase;

class WissKIAlternativeNavigateController extends ControllerBase {

    public function wisskinavigate_page_with_theme() {
    //alle verfügbaren Views mit ihren aktuellen Bezeichnungen und Pfaden werden geladen
    $entity_ids = \Drupal::entityQuery('view')
      ->condition('status', TRUE)
      ->execute();

    $loaded_views = array();

    foreach($entity_ids as $entity_id) {
            $view = \Drupal::service('entity_type.manager')
            ->getStorage('view')
	    ->load($entity_id);

//	    echo '<pre>' , var_dump($view->get('description')), '</pre>';
//	    echo $view->get('display')[$entity_id]['display_options']['path']." - ".$view->label(). " - ".$entity_id;
	    $loaded_views[$entity_id]['label'] = $view->label();
	    $loaded_views[$entity_id]['display'] = $view->get('display');
	    $loaded_views[$entity_id]['administrative_description'] = $view->get('description');
	    
	    if(isset($view->get('display')[$entity_id]['display_options']['path'])) {
                      	    $loaded_views[$entity_id]['path'] = $view->get('display')[$entity_id]['display_options']['path'];
	    }
	    else {
		    if(isset($view->get('display')['page_1'])) {
			    $loaded_views[$entity_id]['path'] = $view->get('display')['page_1']['display_options']['path'];
		    }
	    }

    }

    //Alle eingestellten Views werden geladen
    $config = \Drupal::config('wisski_alternative_navigate.adminsettings');
    $display_views = $config->get('display_views');
    $display_option = $config->get('display_option');

    //The values will be first sorted by weight, then alphabetical
    $view_weight  = array_column($display_views, 'weight');
    $view_original_label = array_column($display_views, 'original_label');
    $view_new_label = array_column($display_views, 'new_label');
    array_multisort( $view_weight, SORT_DESC, $view_new_label, SORT_DESC, $view_original_label, SORT_DESC, $display_views);

    //Die aktivierten Views werden zusammen mit ihren aktuellen Bezeichnungen und Pfaden in ein Array gepackt
    $active_navigate_views = array();

    //The values are shown on the page
    foreach($display_views as $display_view) {
	    if(isset($display_view['view_id'])) {
	        $display_view_id = $display_view['view_id'];
	        if($display_view['activate'] == 1) {
                    if($display_view['new_label'] == ''){
                            $active_navigate_views[$display_view_id]['label'] = $loaded_views[$display_view_id]['label'];
                    }
                    else {
                            $active_navigate_views[$display_view_id]['label'] = $display_view['new_label'];
		    }
		    $page = $display_view['page'];
		    $page_path = $loaded_views[$display_view_id]['display'][$page]['display_options']['path'];
		    
		    $active_navigate_views[$display_view_id]['path'] = $page_path;

		    if(!empty($display_view['preview_image'][0])) {
		      $file = \Drupal\file\Entity\File::load($display_view['preview_image'][0]);
		      $uri = $file->getFileUri();
		      $active_navigate_views[$display_view_id]['preview_image'] = file_create_url($uri);
		    }
		    else {
		      $active_navigate_views[$display_view_id]['preview_image'] = '';	    
		    }

		     $active_navigate_views[$display_view_id]['administrative_description'] = $loaded_views[$display_view_id]['administrative_description'];
	        } 
	    }
    }

    return array(
      //Your theme hook name
      '#theme' => 'wisski_alternative_navigate_page_theme',      
      //Your variables
      '#navigate_item' => $active_navigate_views,
      '#display_option' => $display_option,
    );
  }
}
